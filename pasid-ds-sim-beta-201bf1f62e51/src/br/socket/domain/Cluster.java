package br.socket.domain;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Airton A cluster has several servers. It receives requests directly
 *         from a source or a gateway. Requests received are forwarded to the
 *         respective servers. The server is chosen sequentially and if it is
 *         available
 */
public class Cluster extends Thread {
	private ServerSocket welcomeSokect;
	private Socket connectionSocket;
	private Integer port = null;
	private String name = null;
	private List<Server> servers = null;
	private int counter = 0;
	private List<Long> responseTimes;
	private List<String> requestsQueue;
	private List<Long> requestsQueueTime;
	private Metrics metrics;
	private String contentToProcess;
	private DataInputStream dataInputStream;
	private boolean printLogs;

	private int drops = 0;
	private int queueSize;
	private int req = 0;

	public Cluster(String name, Integer port, Metrics metrics, boolean printLogs) {
		this.name = name;
		this.port = port;
		this.metrics = metrics;
		requestsQueue = new ArrayList<String>();
		requestsQueueTime = new ArrayList<Long>();
		this.printLogs = printLogs;
	}

	public Metrics getMetrics() {
		return this.metrics;
	}

	public synchronized void addTime(long time) {
		this.responseTimes.add(time);
	}

	@Override
	public void run() {
		try {
			this.createConnection();
			dataInputStream = new DataInputStream(connectionSocket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		String receivedFile = null;
		boolean alocated;

		long serverExecTimeIni = 0;

		List<ServerExecutionTime> allExecTimes = new ArrayList<ServerExecutionTime>();

		long totalServerExecTime = 0;
		long totalClusterExecTime = 0;

		long serverID;

		long totalExecTime = System.currentTimeMillis();
		long lastRequesteTime = -1;

		int auxInd;

		while (true) {
			serverExecTimeIni = System.currentTimeMillis();
			StringBuffer log = new StringBuffer();
			alocated = false;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (existsNewRequest()) {
				lastRequesteTime = System.currentTimeMillis();
				//
				req++;
				metrics.setRequests(req);
				queueSize = this.requestsQueue.size();

				if (queueSize < this.metrics.getMaxSizeQueue()) {
					this.metrics.addAcceptedRequest();
					// adicionando tempo inicial de novo cálculo de MRT
					auxInd = (int) Long.parseLong(contentToProcess.substring((contentToProcess.indexOf("#") + 1),
							contentToProcess.indexOf("-")));
					this.metrics.getRequestsInicialTime()[auxInd] = lastRequesteTime;

					requestsQueue.add(contentToProcess);
					requestsQueueTime.add(System.currentTimeMillis());
                    printLogAcceptedRequest(log);
				} else {
					drops++;
					this.metrics.incDropReq();
                    printLogDroppedRequest(log);
				}

				this.contentToProcess = null;
			}

			if (!this.requestsQueue.isEmpty()) {

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (this.isClusterFree()) {

					receivedFile = requestsQueue.remove(0);
					long time = System.currentTimeMillis() - requestsQueueTime.remove(0);
					metrics.addClusterQueuePackageTime(time);

					if (receivedFile != null) {
						for (int i = counter; i < servers.size() && !alocated; i++) {
							if (servers.get(i).isAvailable()) {
								servers.get(i).setContentToProcess(receivedFile);
								alocated = true;

								totalServerExecTime = System.currentTimeMillis() - serverExecTimeIni;
								serverID = servers.get(i).getId();

								allExecTimes.add(new ServerExecutionTime(serverID, totalServerExecTime));
								totalClusterExecTime += totalServerExecTime;

								//System.out.println("Server utilizado: " + servers.get(i).getNameServer());
								//System.out.println("Tempo no cluster: " + (System.currentTimeMillis() - initial));
							}
						}
					}

					counter++;

					if (counter >= servers.size()) {
						counter = 0;
					}
				}

			}
//			try {
//				Thread.sleep(10000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

			// if (req>= metrics.getNumberOfRequests()) {

				if (findouProcessamento()){
                    System.out.println("----------------------");
                	metrics.calculateServerProcessingTimesMap();

                	// Escolha qual modo quer imprimir os resultados
					// metrics.printMetricsLikeTable();
					metrics.printMetricsLikeMap();

				/*
				 * metrics.printExecutionTimes(allExecTimes,
				 * totalClusterExecTime, servers.size());
				 * System.out.println("Execution time cluster: " +
				 * totalClusterExecTime); System.out.println(
				 * "----------------------------------------------"); long
				 * totalTime = System.currentTimeMillis() - totalExecTime; long
				 * totalLevel = (totalTime/totalClusterExecTime) * 100;
				 *
				 * System.out.println("Porcentagem total: " + totalLevel + "%");
				 * System.out.println("Tempo total: " + totalTime);
				 */

//				System.out.println("_-_-_-_-_-_");
//				this.metrics.printNewMRT();

				System.exit(0);
				// }

			}

			if ((System.currentTimeMillis() - lastRequesteTime) > 30000) {
				System.out.println("Inativo por 30 segundos");
				System.exit(0);
			}

			// metrics.printGenerationIntervalDelay();
			// metrics.printServerTimesAlocationMean();
			// metrics.printCoreProcessingTimes();
			//
			// System.out.println("----------------------------------------------");
			// this.metrics.printAcceptReqSize();
			// this.metrics.printDropedReqSize();
			//
			// System.out.println("----------------------------------------------");
			//
			//
			// metrics.printMRT();
			// Toolkit tk = Toolkit.getDefaultToolkit();
			// tk.beep();

			// }

		}
	}

	private boolean findouProcessamento() {
		int req = (int) this.metrics.getNumberOfRequests();
		int rt = this.metrics.getResponseTimes().size();
		// int acc = this.metrics.getAcceptedRequests().intValue();
		int drop = this.metrics.getDroppedRequests().intValue();
		// System.out.println("(" + req + " - " + rt + ") == " + drop);

		if ((req - rt) == drop)
			return true;

		return false;
	}

	private boolean existsNewRequest() {
		try {
			contentToProcess = dataInputStream.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contentToProcess != null;
	}

	private boolean isClusterFree() {
		for (Server s : this.servers) {
			if (s.isAvailable()) {
				return true;
			}
		}
		return false;
	}

	private void createConnection() throws IOException {
		welcomeSokect = new ServerSocket(port);
		connectionSocket = welcomeSokect.accept();
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public List<Server> getServers() {
		return servers;
	}

	public void setServers(List<Server> servers) {
		this.servers = servers;

		for (int i = 0; i < servers.size(); i++) {
			servers.get(i).start();
		}
	}

	public void printLogAcceptedRequest(StringBuffer log){
        if(printLogs) {
            log.append("#Cluster Request: ").append(req)
                    .append("; Queue Size: ").append(queueSize)
                    .append("; Queue Max Size: ").append(this.metrics.getMaxSizeQueue())
                    .append("; Request Accepted");
            System.out.println(log.toString());
        }
    }

    private void printLogDroppedRequest(StringBuffer log) {
        if(printLogs) {
            log.append("#Cluster Request: ").append(req)
                    .append("; Queue Size: ").append(queueSize)
                    .append("; Queue Max Size: ").append(this.metrics.getMaxSizeQueue())
                    .append("; Request Dropped");
            System.out.println(log.toString());
        }
    }
}