package br.socket.domain;

import br.utils.Utils;

/**
 * @author Airton
 * A Server has several Cores. A core represents a processing core of a server.
 */
public class Core extends Thread {

	private String contentToProcess;
	private String name;
	private long inicialTimeOfAllprocess; // When the content left the client
	private Cluster cluster;
	private Metrics metrics;
	//grupo 2
	private TypeServer typeServer;
	private String nameServer;

	public Core(String name, Metrics metrics, TypeServer typeServer, String nameServer) {
		this.name = name;
		this.metrics = metrics;
		//grupo 2
		this.typeServer = typeServer;
		this.nameServer = nameServer;
	}

	public String getContentToProcess() {
		return contentToProcess;
	}

    public void setContentToProcess(String contentToProcess) {
        this.contentToProcess = contentToProcess;
    }

    public boolean isFree() {
        return contentToProcess == null;
    }

	@Override
	public void run() {
		String coreDescription = this.nameServer + "(" + typeServer +  "):" + name;
//		System.out.println(coreDescription + " Starting ...");
		long responseTime=0;
		
		//variavel para novo MRT
		int auxInd;
		
		while (true) {
//			System.out.println(coreDescription + " Waiting ...");

            try {
			    Thread.sleep(5);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			}

			if (this.contentToProcess != null) {
//				System.out.println("\t" + coreDescription + " Starting Process ...");

				int indexLastServerTime = metrics.getServerAllocationTimes().size()-1;
				long lastServerTime = metrics.getServerAllocationTimes().get(indexLastServerTime);
				metrics.getServerAllocationTimes().set(indexLastServerTime, System.currentTimeMillis() - lastServerTime);

				inicialTimeOfAllprocess = Long.parseLong(contentToProcess.substring(0,contentToProcess.indexOf("#")));
                long startProcess = System.currentTimeMillis();

				//Este processamento poder ser adaptado aos seus interesses espec�ficos
				for (int i = 0; i < 10000; i++) {
					//try {Thread.sleep(2);} catch (InterruptedException e) {e.printStackTrace();}
					Utils.wordCount(contentToProcess);
				}

				long endProcess = System.currentTimeMillis();

				long processTime = endProcess - startProcess;
				this.metrics.getCoreProcessingTimes().add(processTime);
				this.metrics.getServerProcessingTimesMap().put(this.typeServer.name()+"_"+this.nameServer, processTime);


				responseTime = endProcess - inicialTimeOfAllprocess;

//				System.out.println(coreDescription + " Starting Process ++++...");

				this.metrics.getResponseTimes().add(responseTime);

				//nova lógica de incluir MRT
				auxInd = (int) Long.parseLong(contentToProcess.substring((contentToProcess.indexOf("#")+1), contentToProcess.indexOf("-")));
				this.metrics.getRequestsFinalTime()[auxInd] = endProcess;
				//
				
				contentToProcess = null;
//				System.out.println("\t:::"+ coreDescription + "Process Time: " + processTime + "; Response Time: " + responseTime);
			}
		}
	}
}
