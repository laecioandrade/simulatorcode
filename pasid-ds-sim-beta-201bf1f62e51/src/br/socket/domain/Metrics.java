package br.socket.domain;

import java.util.*;

public class Metrics {
	private List<Long> responseTimes;
	private List<Long> serverAllocationTimes;
	private List<Long> coreProcessingTimes;
	private long generationIntervalDelay;
	private long numberOfRequests;
	private long maxSizeQueue;
	private long droppedRequests =0;
	private long acceptedRequests =0;
	private double clusterAveragePacketQueuingTime;
	private List<Long> clusterPackageQueueTimes;
	private HashMap<String, Long> serverProcessingTimesMap, serverIdleTimesMap;
	
	private Long[] requestsInicialTime, requestsFinalTime;
	//private HashMap<Long, Long> requestsInicialTime, requestsFinalTime;
	
	private List<Long> serverProcessingTimes;
	private List<Long> serverIdleTimes;
	private long idleConsumptionC; //tempo do servidor quando ocioso
	private double averageRunningConsumptionComputer; //tempo do servidor tipo computador quando em atividade
	private double averageRunningConsumptionSmartphone; //tempo do servidor tipo smartphone quando em atividade
	private long idleConsumptionS; //tempo do servidor quando ocioso
	private long generalConsumptionIdle; //tempo geral dos servidores quando ociosos
	private double generalConsumptionRunning; //tempo geral dos servidores quando em atividade
	private double mrt;
	private int requests = 0;
	private int numComputerServer;
	private int numSmartphoneServer;

	public Metrics() {
		responseTimes = new ArrayList<Long>();
		serverAllocationTimes = new ArrayList<Long>();
		coreProcessingTimes = new ArrayList<Long>();
		clusterPackageQueueTimes = new ArrayList<Long>();

		serverProcessingTimesMap = new HashMap<String, Long>();
		serverIdleTimesMap = new HashMap<String, Long>();
		
		//serverProcessingTimes = new ArrayList<Long>();
		//serverIdleTimes = new ArrayList<Long>();
	}

	public Long[] getRequestsInicialTime() {
		return requestsInicialTime;
	}

	public void setRequestsInicialTime(Long[] requestsInicialTime) {
		this.requestsInicialTime = requestsInicialTime;
	}

	public Long[] getRequestsFinalTime() {
		return requestsFinalTime;
	}

	public void setRequestsFinalTime(Long[] requestsFinalTime) {
		this.requestsFinalTime = requestsFinalTime;
	}

	public HashMap<String, Long> getServerIdleTimesMap() {
		return serverIdleTimesMap;
	}

	public void setServerIdleTimesMap(HashMap<String, Long> serverIdleTimesMap) {
		this.serverIdleTimesMap = serverIdleTimesMap;
	}

	public HashMap<String, Long> getServerProcessingTimesMap() {
		return serverProcessingTimesMap;
	}

	public void setServerProcessingTimesMap(HashMap<String, Long> serverProcessingTimesMap) {
		this.serverProcessingTimesMap = serverProcessingTimesMap;
	}


	public int getRequests(){
		return this.requests;
	}

	public void setRequests(int r){
		this.requests = r;
	}

	public void addRequest(){
		this.requests++;
	}

	public void setMaxSizeQueue(long t){
		this.maxSizeQueue = t;
	}
	
	public void setMaxSizeQueue(int t){
		this.maxSizeQueue = Long.valueOf(t);
	}
	
	public Long getMaxSizeQueue(){
		return this.maxSizeQueue;
	}
	
	public void addAcceptedRequest(){
		this.acceptedRequests++;
	}
	
	public Long getAcceptedRequests(){
		return this.acceptedRequests;
	}
	
	public void setDroppedRequests(long d){
		this.droppedRequests = d;
	}

    public double getClusterAveragePacketQueuingTime() {
		long sum = 0;
		for (Long time : this.clusterPackageQueueTimes) {
			sum += time.longValue();
		}
		this.clusterAveragePacketQueuingTime = (double) sum / (double) this.clusterPackageQueueTimes.size();

		return this.clusterAveragePacketQueuingTime;
    }

    public void setClusterAveragePacketQueuingTime(double clusterAveragePacketQueuingTime) {
        this.clusterAveragePacketQueuingTime = clusterAveragePacketQueuingTime;
    }

    public List<Long> getClusterPackageQueueTimes() {
        return clusterPackageQueueTimes;
    }

    public void setClusterPackageQueueTimes(List<Long> clusterPackageQueueTimes) {
        this.clusterPackageQueueTimes = clusterPackageQueueTimes;
    }

    public void addClusterQueuePackageTime(long packageTime) {
        this.clusterPackageQueueTimes.add(packageTime);
    }

    public void incDropReq() {
        this.droppedRequests++;
    }

    public Long getDroppedRequests() {
        return this.droppedRequests;
    }

    public long getNumberOfRequests() {
        return numberOfRequests;
    }

    public void setNumberOfRequests(long numberOfRequests) {
        this.numberOfRequests = numberOfRequests;
    }

    public long getGenerationIntervalDelay() {
        return generationIntervalDelay;
    }

    public void setGenerationIntervalDelay(long generationIntervalDelay) {
        this.generationIntervalDelay = generationIntervalDelay;
    }

    public List<Long> getResponseTimes() {
        return responseTimes;
    }

	public void setResponseTimes(List<Long> responseTimes) {
		this.responseTimes = responseTimes;
	}

	public double getMRT(){
		long sum = 0;
		for (Long long1 : responseTimes) {
			sum += long1;
		}
		this.mrt = (double) sum / (double) responseTimes.size();

		return this.mrt;
	}

	public void printMRT(){
		System.out.println("MRT:" + this.getMRT());
	}

	public double getNewMRT(){
		Long ini, f;
		Long sum = (long) 0, req = (long) 0 ;
		for (int i = 0; i < this.numberOfRequests; i++) {
			ini = this.requestsInicialTime[i];
			f = this.requestsFinalTime[i];

			if(ini != null && f !=null){
				sum = sum+(f-ini);
				req++;
//				System.out.println("Dif Req#"+i+" - "+(f-ini));
			}
//			System.out.println("Req#"+i+" - "+(f + ", " + ini));
		}
		return ((double) sum/ (double)req);
	}

	public void printNewMRT(){
		System.out.println("New MRT: " + getNewMRT());
	}

	public List<Long> getServerAllocationTimes() {
		return serverAllocationTimes;
	}

	public void setServerAllocationTimes(List<Long> serverAllocationTimes) {
		this.serverAllocationTimes = serverAllocationTimes;
	}

	public double getServerAllocationAverageTimes(){
		long sum = 0;
		for (Long long1 : serverAllocationTimes) {
			sum += long1;
		}
		return (double)sum / (double)serverAllocationTimes.size();
	}
	public void printtServerAllocationAverageTimes(){
		System.out.println("Server Allocation Average Times:" + getServerAllocationAverageTimes());
	}

	public List<Long> getCoreProcessingTimes() {
		return coreProcessingTimes;
	}

	public void setCoreProcessingTimes(List<Long> coreProcessingTimes) {
		this.coreProcessingTimes = coreProcessingTimes;
	}

	public double getCoreProcessingAverageTimes(){
		long sum = 0;
		for (Long long1 : coreProcessingTimes) {
			sum += long1;
		}
		return (double) sum/ (double)coreProcessingTimes.size();
	}

	public void printCoreProcessingAverageTimes(){
		System.out.println("Core Processing Average Times:" + getCoreProcessingAverageTimes());
	}

	public void calculateServerProcessingTimesMap(){ //lista com tempo de processo de cada servidor
		int countComputers = 0;
		int countSmartphones = 0;
		long sumComputersTimes = 0;
		long sumSmartphonesTimes = 0;

		for(String key:serverProcessingTimesMap.keySet()) {
			if(key.contains(TypeServer.COMPUTER.name())) {
				sumComputersTimes += serverProcessingTimesMap.get(key);
				countComputers++;
			}else {
				sumSmartphonesTimes += serverProcessingTimesMap.get(key);
				countSmartphones++;
			}
		}

		long computerServerTimeInSecond = sumComputersTimes/1000;
		long smartphoneServerTimeInSecond = sumSmartphonesTimes/1000;

		averageRunningConsumptionComputer = (computerServerTimeInSecond*TypeServer.COMPUTER.getMax())/countComputers;
		averageRunningConsumptionSmartphone = (smartphoneServerTimeInSecond*TypeServer.COMPUTER.getMax())/countComputers;

		generalConsumptionRunning = averageRunningConsumptionComputer + averageRunningConsumptionSmartphone;

		//System.out.println("Server Processing Times: " + serverProcessingTimesMap); //dados
//		this.averageRunningConsumptionComputer = (long) ((sumComputersTimes*TypeServer.COMPUTER.getMax())/countComputers)/3600;
//		float result1 = (float) this.averageRunningConsumptionComputer;
//		System.out.printf("\nServer Processing Consumer Computer Average (Wh): %.5f", result1);
//		this.averageRunningConsumptionSmartphone = (long) ((sumSmartphonesTimes*TypeServer.SMARTPHONE.getMax())/countSmartphones)/3600;
//		float result2 = (float) this.averageRunningConsumptionSmartphone;
//		System.out.printf("\nServer Processing Consumer Smartphone Average (Wh): %.5f", result2);
//		float result3 = (float) generalConsumptionRunning/3600;
//		System.out.printf("\nServer Processing Consumer General (Wh): %.5f \n", result3); //media geral
		//System.out.println("Server Processing Consumer General (Ws): " + ((sumComputersTimes+sumSmartphonesTimes)/serverProcessingTimesMap.size())*consumer); //media geral
	}

	public void printServerIdleTimesMap(){ //lista com tempo de processo de cada servidor
		int contC = 0, contS = 0;
		Long sumC = (long) 0; Long sumS = (long) 0;

		if(serverIdleTimesMap.size() > 0) {
			for(String key:serverIdleTimesMap.keySet()) {
				if(key.contains(TypeServer.COMPUTER.name())) {
					sumC += serverIdleTimesMap.get(key);
					contC += 1;
				}else {
					sumS += serverIdleTimesMap.get(key);
					contS += 1;
				}

			}
		}

		generalConsumptionIdle = (long) ((((sumC*TypeServer.COMPUTER.getMin()))+((sumS*TypeServer.SMARTPHONE.getMin()))))/(contC+contS);

		//System.out.println("Server Idle Times: " + serverIdleTimesMap); //dados
		idleConsumptionC = (long) ((sumC*TypeServer.COMPUTER.getMin())/contC)/3600;
		float result1 = (float) idleConsumptionC;
		System.out.printf("\nServer Idle Consumer Computer Average (Wh): %.5f", result1);
		idleConsumptionS = (long) ((sumS*TypeServer.SMARTPHONE.getMin())/contS)/3600;
		float result2 = (float) idleConsumptionS;
		System.out.printf("\nServer Idle Consumer Smartphone Average (Wh): %.5f", result2);
		float result3 = (float) generalConsumptionIdle/3600;
		System.out.printf("\nServer Idle Consumer General (Wh): %.5f \n", result3); //media geral
		//System.out.println("Server Idle Consumer General (Ws): " + ((sumC+sumS)/serverIdleTimesMap.size())*consumer); //media geral
	}

	public void printDropedReqSize(){
		double d = getDroppedRequests().intValue();
		double r = this.numberOfRequests;

		double p = d/r*100;

		System.out.println("Number of dropped requests: " + d + " ("+p+"% of total.)");
	}

	public double getDropedRequestsPercent(){
		double d = getDroppedRequests().intValue();
		double r = this.numberOfRequests;

		double p = d/r*100;

		return p;
	}

	public void printAcceptReqSize(){
		System.out.println("Number of accepted requests: " + getAcceptedRequests());
	}

    public void printGenerationIntervalDelay() {
        System.out.println("Generation Interval Delay: " + generationIntervalDelay);
    }

    public void printClusterAveragePacketQueuingTime() {
        System.out.println("Cluster Average Packet Queuing Time: " + this.clusterAveragePacketQueuingTime);
    }

    public void printExecutionTimes(List<ServerExecutionTime> allExecTimes, long clusterExecTime, int nOfServers){
        float utilizationLevel = 0;
        String strOut;
        int count = 0;
        float level=0;
        float current = 0;

        List<Float> totalExecTimes = new ArrayList<Float>();

        System.out.println("-------Utilization level-------");

        for (int j=0; j < allExecTimes.size(); j++) {
            current = allExecTimes.get(j).getServerID();

            for (int i = 0; i < allExecTimes.size(); i++) {

                if (allExecTimes.get(i).getServerID() == current) {
                    utilizationLevel += allExecTimes.get(i).getExecutionTime();
                    count++;

                }
            }
            totalExecTimes.add(utilizationLevel);
            utilizationLevel = 0;
        }

        Set<Float> finalExecTimes = new LinkedHashSet<>(totalExecTimes);

        nOfServers = finalExecTimes.size();

        for (int i = 0; i < nOfServers; i++) {
            level = (totalExecTimes.get(i) / clusterExecTime) * 100;
            // level = totalExecTimes.get(i);
            strOut = String.format("Utilization level server %d: %.1f%%", allExecTimes.get(i).getServerID(), level);
            System.out.println(strOut + " " + "(" + totalExecTimes.get(i) + ")");
        }
        System.out.println("-------------------------------");
    }

	public List<Long> getServerProcessingTimes() {
		return serverProcessingTimes;
	}
	public void setServerProcessingTimes(List<Long> serverProcessingTimes) {
		this.serverProcessingTimes = serverProcessingTimes;
	}

	public List<Long> getServerIdleTimes() {
		return serverIdleTimes;
	}

	public void setServerIdleTimes(List<Long> serverIdleTimes) {
		this.serverIdleTimes = serverIdleTimes;
	}

	public long getIdleConsumptionC() {
		return idleConsumptionC;
	}

	public void setIdleConsumptionC(long idleConsumptionC) {
		this.idleConsumptionC = idleConsumptionC;
	}

	public void printAverageRunningConsumptionComputerInWs(){
		System.out.printf("Average Running Consumption Computer (Ws): %.5f\n", this.getAverageRunningConsumptionComputer());
	}

	public void printAverageRunningConsumptionComputerInWh(){
		System.out.printf("Average Running Consumption Computer (Wh): %.5f\n", this.getAverageRunningConsumptionComputerInWh());
	}

	public double getAverageRunningConsumptionComputerInWh(){
		return averageRunningConsumptionComputer/3600;
	}

	public double getAverageRunningConsumptionComputer() {
		return averageRunningConsumptionComputer;
	}

	public void setAverageRunningConsumptionComputer(double averageRunningConsumptionComputer) {
		this.averageRunningConsumptionComputer = averageRunningConsumptionComputer;
	}

	public void printAverageRunningConsumptionSmartphoneInWs(){
		System.out.printf("Average Running Consumption Smartphone (Ws): %.5f\n", this.getAverageRunningConsumptionSmartphone());
	}

	public void printAverageRunningConsumptionSmartphoneInWh(){
		System.out.printf("Average Running Consumption Smartphone (Wh): %.5f\n", this.getAverageRunningConsumptionSmartphoneInWh());
	}

	public double getAverageRunningConsumptionSmartphoneInWh(){
		return averageRunningConsumptionSmartphone/3600;
	}

	public double getAverageRunningConsumptionSmartphone() {
		return averageRunningConsumptionSmartphone;
	}

	public void setAverageRunningConsumptionSmartphone(double averageRunningConsumptionSmartphone) {
		this.averageRunningConsumptionSmartphone = averageRunningConsumptionSmartphone;
	}

	public long getIdleConsumptionS() {
		return idleConsumptionS;
	}

	public void setIdleConsumptionS(long idleConsumptionS) {
		this.idleConsumptionS = idleConsumptionS;
	}

    public int getNumComputerServer() {
        return numComputerServer;
    }

    public void setNumComputerServer(int numComputerServer) {
        this.numComputerServer = numComputerServer;
    }

    public int getNumSmartphoneServer() {
        return numSmartphoneServer;
    }

    public void setNumSmartphoneServer(int numSmartphoneServer) {
        this.numSmartphoneServer = numSmartphoneServer;
    }

	/**
	 * Creates a map with metrics to print as needed. New metrics must be adder as needed.
	 *
	 * */
	public Map<String, String> mapMetricsToPrint() {
		Map<String, String> mapMetrics = new LinkedHashMap<>();

		mapMetrics.put("numComputerServer", String.valueOf(numComputerServer));
		mapMetrics.put("numSmartphoneServer", String.valueOf(numSmartphoneServer));
		mapMetrics.put("generationIntevalDelay", String.valueOf(generationIntervalDelay));
		mapMetrics.put("generationRate", String.format("%.3f\t", (1.0d/generationIntervalDelay)));
		mapMetrics.put("MaxSizeQueue", String.valueOf(maxSizeQueue));
//		mapMetrics.put("MRT", String.valueOf(mrt));
		mapMetrics.put("newMRT", String.format("%.3f\t", getNewMRT()));
		mapMetrics.put("numberOfRequests", String.valueOf(numberOfRequests));
		mapMetrics.put("AcceptedRequests", String.valueOf(acceptedRequests));
		mapMetrics.put("DroppedRequests", String.valueOf(droppedRequests));
		mapMetrics.put("DroppedRequestsPercent", String.format("%.3f\t", getDropedRequestsPercent()));
		mapMetrics.put("ClusterAveragePacketQueuingTime", String.valueOf(clusterAveragePacketQueuingTime));
		mapMetrics.put("averageRunningConsumptionComputerInWh", String.valueOf(getAverageRunningConsumptionComputerInWh()));
		mapMetrics.put("averageRunningConsumptionSmartphoneInWh", String.valueOf(getAverageRunningConsumptionSmartphoneInWh()));

		return mapMetrics;
	}

	/**
	 * Get values from a metric group and makes a string to print as a table header
	 *
	 * */

	public String getMetricsHeaderLikeRow(){
        StringBuffer header = new StringBuffer();

		Map<String, String> mapMetrics = mapMetricsToPrint();

        for(Map.Entry<String, String> metric: mapMetrics.entrySet()) {
			header.append(metric.getKey()).append("\t");
		}
        return header.toString();
    }

	/**
	 * Get values from a metric group and makes a string to print as a table row
	 *
	 * */
    public String getMetricsBodyLikeRow(){
        StringBuffer body = new StringBuffer();

		Map<String, String> mapMetrics = mapMetricsToPrint();

		for(Map.Entry<String, String> metric: mapMetrics.entrySet()) {
			body.append(metric.getValue()).append("\t");
		}
        return body.toString();
	}

	/**
	 * Prints a group of metrics as a single-row table
	 *
	 * */
	public void printMetricsLikeTable(){
        System.out.println(getMetricsHeaderLikeRow());
        System.out.println(getMetricsBodyLikeRow());
    }

	public void printMetricsLikeMap(){
		StringBuffer stringBuffer = new StringBuffer();

		Map<String, String> mapMetrics = mapMetricsToPrint();

		for(Map.Entry<String, String> metric: mapMetrics.entrySet()) {
			stringBuffer.append(metric.getKey()).append(": ").append(metric.getValue()).append("\n");
		}
		System.out.println(stringBuffer.toString());
	}

}
