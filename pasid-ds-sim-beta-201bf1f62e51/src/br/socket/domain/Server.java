package br.socket.domain;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread{
	private ServerSocket welcomeSokect;
	private Socket connectionSocket;
	private Integer port = null;
	private String nameServer;
	private List<Core> cores;
	private int coresNumber;
	private String contentToProcess;
	private int counter = 0;
	private List<String> requestsQueue;
	private TypeServer typeServer;
    private long idleConsumption;
    private long runningConsumption;
    private Metrics metrics;

    public Server(String name, int coresNumber, TypeServer type, Metrics metrics) {
		this.nameServer = name;
		this.coresNumber = coresNumber;
		this.typeServer = type;
		this.metrics = metrics;
		this.cores = new ArrayList<Core>();
		this.requestsQueue = new ArrayList<String>();

		if(type == TypeServer.COMPUTER){
			this.metrics.setNumComputerServer(this.metrics.getNumComputerServer() + 1);
		} else {
			this.metrics.setNumSmartphoneServer(this.metrics.getNumSmartphoneServer() + 1);
		}

		for (int i = 0; i < coresNumber; i++) {
			cores.add(new Core("core"+(i+1), metrics, type, this.nameServer));
			cores.get(i).start();
		}
	}

	public TypeServer getTypeServer() {
		return typeServer;
	}

	public long getIdleConsumption() {
		return idleConsumption;
	}

	public void setIdleConsumption(long idleConsumption) {
		this.idleConsumption = idleConsumption;
	}

	public long getRunningConsumption() {
		return runningConsumption;
	}

	public void setRunningConsumption(long runningConsumption) {
		this.runningConsumption = runningConsumption;
	}

	public String getNameServer() {
		return nameServer;
	}

	public void setNameServer(String nameServer) {
		this.nameServer = nameServer;
	}

	public String getContentToProcess() {
		return contentToProcess;
	}

	public void setContentToProcess(String contentToProcess) {
		this.contentToProcess = contentToProcess;
	}

	public boolean isAvailable() {
		for (Core core : cores) {
			if (core.getContentToProcess() == null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existsNewRequest() {
		return this.contentToProcess != null;
	}

	@Override
	public void run() {
		//idleConsumption = System.currentTimeMillis(); //inicia timer idle
		String receivedFile = null;
		boolean alocated = false;
		
		//long initial = System.currentTimeMillis(); //inicia timer process
		while (true) {
			alocated = false;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (existsNewRequest()) {
				requestsQueue.add(contentToProcess);
				this.contentToProcess = null;
			}

			if (!this.requestsQueue.isEmpty()) {
				// try {Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();}

				if (this.isAvailable()) {
					receivedFile = requestsQueue.remove(0);
					if (receivedFile != null) {
						for (int i = counter; i < cores.size() && !alocated; i++) {
							if (cores.get(i).isFree()) {
								cores.get(i).setContentToProcess(receivedFile);
								alocated = true;
								//System.out.println("Tempo no servidor " + (System.currentTimeMillis() - initial));
							}
						}
					}

					counter++;

					if (counter >= cores.size()) {
						counter = 0;
					}
					idleConsumption = System.currentTimeMillis(); //inicia timer idle
				}
			}
			idleConsumption = System.currentTimeMillis() - idleConsumption;	//captura o intervalo idle
			metrics.getServerIdleTimesMap().put(this.typeServer.name()+"_"+this.nameServer, idleConsumption);
		}
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

}