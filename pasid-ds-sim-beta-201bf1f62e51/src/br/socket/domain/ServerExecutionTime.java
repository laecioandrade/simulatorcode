package br.socket.domain;

public class ServerExecutionTime {
    private long serverID;
    private long executionTime;

    public ServerExecutionTime(long serverID, long totalServerExecTime) {
        this.serverID = serverID;
        this.executionTime = totalServerExecTime;
    }

    public long getServerID() {
        return serverID;
    }

    public void setServerID(long serverID) {
        this.serverID = serverID;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }
}