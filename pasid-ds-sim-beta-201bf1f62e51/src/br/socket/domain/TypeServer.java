package br.socket.domain;

public enum TypeServer {
	//*Obs: 1 watt hora = 3 600 watts por segundo <-> 1 watt segundo = 0,00027777777777778 watt hora
	//COMPUTER("Computer", 1.7, 23.6),
	//SMARTPHONE("Smartphone", 0.078, 3.3); //Valores em Wh
	COMPUTER("Computer", 6120, 84960),
	SMARTPHONE("Smartphone", 280.8, 11880); //Valores em Ws
	private String name;
	private double min;
	private double max;
		
	private TypeServer(String name, double min, double max) {
		this.name = name;
		this.min = min;
		this.max = max;
	}

	public String getName() {
		return name;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	
	
	
}
