package main;

import br.socket.domain.*;

import java.util.ArrayList;
import java.util.List;

public class SimulationStart {

	public static void main(String[] args) throws InterruptedException {

		int numberOfRequests = 30;
		int qtdCores = 8;

		int generationIntevalDelay = 10; // taxa de geração = 1/10 = 0.1

		// taxa de gereção = 1/generationIntevalDelay => lambda
		double taxa = 1.0d/generationIntevalDelay;

		Metrics metrics = new Metrics();
		metrics.setGenerationIntervalDelay(generationIntevalDelay);
		metrics.setNumberOfRequests(numberOfRequests);
		//
		
		metrics.setRequestsInicialTime(new Long[numberOfRequests]);
		metrics.setRequestsFinalTime(new Long[numberOfRequests]);
		
		metrics.setMaxSizeQueue(3);
		//
		Cluster cluster = null;
		cluster = new Cluster("cluster1", 3000, metrics, true);

		Source source1 = new Source("source1", 3000, metrics);

		List<Server> servers = new ArrayList<Server>();
		servers.add(new Server("fog-1", qtdCores, TypeServer.COMPUTER, metrics)); // cloud1
		servers.add(new Server("cloud-2", qtdCores, TypeServer.COMPUTER, metrics)); // cloud2
		cluster.setServers(servers);

		System.out.print(generationIntevalDelay + "\t");

		cluster.start();
		source1.start();

	}
}
